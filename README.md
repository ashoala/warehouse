This is a simple project built in Java 8 for warehouse software.

**Project Overview:**

- Java 8
- Spring MVC
- Maven
- JUnit5
- JSP
- JQuery/Ajax


**Model**

![picture](Model.png)

Visit http://localhost:8080/Warehouse/home.jsp

![picture](home.png)

