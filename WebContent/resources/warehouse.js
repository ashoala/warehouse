function sellProduct(productId) {
 
	var productObj = {
	    "productId": productId,
	    "count": $("#"+productId+" :selected").val()
	};
	
 $.ajax({
     type : 'POST',
     url : '/Warehouse/sellProduct',
     dataType : 'json',
     data : productObj,
     success : function(data) {
        updateProductsTable(data);
     },
     error : function() {
         alert('error');
     }
 });
 
 
};


function updateProductsTable(productsList) {
    var rows = '<tr> <th>Product Name </th> <th>Quantity Avaliable</th> <th>Sell</th> </tr> '
    $.each(productsList, function(index, item) {
	        var row;
	        
	        if(item['count']<1){
	        	row = '<tr class="soldOut">';
	        }else{
	        	row  = '<tr>';
	        }
       
            row += '<td>' + item['name'] + '</td>';
            row += '<td>' + item['count'] + '</td>';
            
            var counter = 1;
            row += '<td>';
            
            if(item['count']<1){
            	
            	row += '<div >Sold Out</div>';
            	
            }else{
                row += ' <div> <select id="'+item['id']+'">';
                while(counter<=item['count']){
                	row += '<option value='+counter+'>'+counter+'</option>';
                	counter++;
                }
                row += '<\select>';
                row += '<button onclick="sellProduct('+item['id']+')">Sell</button></div>';
            }

            row += '</td>';
      		rows += row;
    });
    $('#productsTableID').html(rows);
}