<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>


<spring:url value="/resources/warehouse.css" var="warehouseCSS" />
<spring:url value="/resources/warehouse.js" var="warehouseJS" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="${warehouseCSS}" rel="stylesheet" />
<script src="${warehouseJS}" ></script>

</head>

<body class="Content">
    <div class="Info">
        <H1>Products</H1>
    </div>
    <div class="GameContainer">
    
    <table id="productsTableID" width="100%">
    
      <tr>
	    <th>Product Name </th>
	    <th>Quantity Avaliable</th>
	    <th></th>
  	 </tr> 
     <c:forEach items="${products}" var="product">
     <c:set var="productCount" value="${product.count}"/>
     <% if((int)pageContext.getAttribute("productCount")<1){ %>
      	<tr class="soldOut">
      	<% }else{ %>
      		<tr>
  		<% }%>
	        
          <td>${product.name}</td>  
          <td>${product.count}</td> 
          
          <td> 
          <% if((int)pageContext.getAttribute("productCount")<1){ %>
          <div >Sold Out</div>
          <% }else{ %>
          		<div>
          		
          		
          		<select id="${product.id}">
          		
				
          		<% int counter=1; 
          		while ( counter <= (int)pageContext.getAttribute("productCount")){%> 
			         
						<option value="<%= counter %>"><%= counter %></option>
						
			        <% counter++;
			      }
			      %>
          		
				</select>
				
				<button onclick="sellProduct('${product.id}')">Sell</button>
				
          		</div>
          		<% }%>
          		
          </td>
                                     
     </tr>
     </c:forEach>
	 </table>

    
    </div>
    
</body>

</html>