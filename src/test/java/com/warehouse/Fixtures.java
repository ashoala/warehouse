package test.java.com.warehouse;

import com.github.javafaker.Faker;
import main.java.com.warehouse.models.Article;
import main.java.com.warehouse.models.Product;
import main.java.com.warehouse.models.Requirement;

import java.util.List;
import java.util.stream.Collectors;

public class Fixtures {

    private final static Faker faker = new Faker();

    public static Article buildArticle() {
        int id = faker.number().randomDigitNotZero();
        return new Article(id, "article_"+id, faker.number().numberBetween(5, 10));
    }

    public static Requirement buildRequirement(Product product, Article article, int amount) {
        return new Requirement(product.getId(), article.getId(), amount);
    }

    public static Product buildProduct(List<Article> articles) {
        int productId = faker.number().randomDigitNotZero();
        List<Requirement> requirements = articles.stream()
                .map(article -> new Requirement(productId, article.getId(), faker.number().numberBetween(1, 5)))
                .collect(Collectors.toList());
        return new Product(productId, "product_"+productId, requirements);
    }
}
