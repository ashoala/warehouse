package test.java.com.warehouse;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.warehouse.ProductManager;
import main.java.com.warehouse.WarehouseDB;
import main.java.com.warehouse.date_importing.DataImporter;
import main.java.com.warehouse.models.Product;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductManagerTest {
    private WarehouseDB warehouseDB;
    private ProductManager productManager;

    @BeforeEach
    public void setup() throws IOException {
        this.warehouseDB = WarehouseDB.getInstance();
        this.productManager = new ProductManager(warehouseDB);
        loadDate();
    }

    private void loadDate() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        DataImporter dataImporter = new DataImporter(objectMapper, warehouseDB);
        dataImporter.importArticlesFromFile("inventory.json");
        dataImporter.importProductsFromFile("products.json");

        /*
        Articles:
        - leg -> 12
        - screw -> 24
        - seat -> 2
        - table top -> 1

        Products:
        - Dinning Chair(id = 1) -> [leg -> 4] & [screw -> 8] & [seat -> 1]
        - Dinning Table(id = 2) -> [leg -> 4] & [screw -> 9] & [table top -> 1]
        */
    }

    @Test
    public void syncProductTest() {
        Product product1 = warehouseDB.getProduct(1);
        productManager.syncProductCount(product1);
        Assert.assertEquals(2, product1.getCount());

        Product product2 = warehouseDB.getProduct(2);
        productManager.syncProductCount(product2);
        Assert.assertEquals(1, product2.getCount());
    }

    @Test
    public void sellProductTest() {
        Product product1 = warehouseDB.getProduct(1);
        Product product2 = warehouseDB.getProduct(2);
        productManager.syncProductCount(product1);
        productManager.syncProductCount(product2);

        productManager.sellProduct(product1, 1);
        Assert.assertEquals(1,  product1.getCount());
        Assert.assertEquals(1,  product2.getCount());

        productManager.sellProduct(product1, 1);
        Assert.assertEquals(0,  product1.getCount());
        // Selling is effecting the other product availability as no enough articls for it
        Assert.assertEquals(0,  product1.getCount());

        assertThrows(IllegalArgumentException.class, () -> productManager.sellProduct(product1, 1));
    }
}
