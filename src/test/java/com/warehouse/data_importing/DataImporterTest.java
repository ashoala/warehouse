package test.java.com.warehouse.data_importing;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.warehouse.WarehouseDB;
import main.java.com.warehouse.date_importing.DataImporter;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;

public class DataImporterTest {
    private WarehouseDB warehouseDB;
    private ObjectMapper objectMapper;
    private DataImporter dataImporter;

    @BeforeEach
    public void setup() {
        this.warehouseDB = WarehouseDB.getInstance();
        this.objectMapper = new ObjectMapper();
        this.dataImporter = new DataImporter(objectMapper, warehouseDB);
    }

    @Test
    public void importArticlesFromFileTest() throws IOException {
        dataImporter.importArticlesFromFile("inventory.json"); // load 4 articles
        Assert.assertEquals(4, warehouseDB.listArticles().size());
    }

    @Test
    public void importProductsFromFileTest() throws IOException {
        dataImporter.importProductsFromFile("products.json");  // load 2 products with 2 articles for each
        Assert.assertEquals(2, warehouseDB.listProducts().size());

        // Test auto-increment id
        Assert.assertNotNull(warehouseDB.getProduct(1));
        Assert.assertNotNull(warehouseDB.getProduct(2));
        Assert.assertNull(warehouseDB.getProduct(3));

        Assert.assertEquals(3, warehouseDB.getProduct(1).getRequirements().size());
    }
}
