package test.java.com.warehouse;

import main.java.com.warehouse.WarehouseDB;
import main.java.com.warehouse.models.Article;
import main.java.com.warehouse.models.Product;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

public class WarehouseDbTest {
    private WarehouseDB dbTest;

    @BeforeEach
    public void setup() {
        this.dbTest = WarehouseDB.getInstance();
    }

    @Test
    public void testArticleInsertion() {
        Article article = Fixtures.buildArticle();

        dbTest.insertArticle(article);
        assertThrows(IllegalArgumentException.class, () -> dbTest.insertArticle(article));

        Assert.assertEquals(article, dbTest.getArticle(article.getId()));
        Assert.assertEquals(1,  dbTest.listArticles().size());

        List<Article> articles = new ArrayList<>();
        articles.add(article);
        Product product = Fixtures.buildProduct(articles);

        dbTest.insertProduct(product);
        assertThrows(IllegalArgumentException.class, () -> dbTest.insertProduct(product));

        Assert.assertEquals(product, dbTest.getProduct(product.getId()));
        Assert.assertEquals(1, dbTest.getProduct(product.getId()).getRequirements().size());
        Assert.assertEquals(1,  dbTest.listProducts().size());
        Assert.assertEquals(1, dbTest.productsSize());
    }

}
