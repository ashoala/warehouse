package main.java.com.warehouse.date_importing;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.com.warehouse.date_importing.requests.articles.ArticleRequest;
import main.java.com.warehouse.date_importing.requests.articles.InventoryRequest;
import main.java.com.warehouse.date_importing.requests.products.ProductRequest;
import main.java.com.warehouse.date_importing.requests.products.ProductsRequest;
import main.java.com.warehouse.models.Article;
import main.java.com.warehouse.models.Product;
import main.java.com.warehouse.WarehouseDB;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.ClassPathResource;

public class DataImporter {
	
    private final ObjectMapper objectMapper;
    private final WarehouseDB warehouseDB;
    
    public DataImporter(ObjectMapper objectMapper, WarehouseDB warehouseDB) {
    	this.objectMapper = objectMapper;
    	this.warehouseDB = warehouseDB;
    }

    public void importArticlesFromFile(String filePath) throws IOException {
        InputStream inputStream = new ClassPathResource(filePath).getInputStream();
        InventoryRequest inventoryRequest = objectMapper.readValue(inputStream, InventoryRequest.class);
        for (ArticleRequest articleRequest : inventoryRequest.getArticleRequests()) {
            warehouseDB.insertArticle(Article.buildFromArticleRequest(articleRequest));
        }
    }

    public void importProductsFromFile(String filePath) throws IOException {
    	InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filePath);
        ProductsRequest productsRequest = objectMapper.readValue(inputStream, ProductsRequest.class);
        for (ProductRequest productRequest : productsRequest.getProductRequests()) {
        	Product product = Product.buildFromProductRequest(warehouseDB.productsSize() + 1, productRequest);
            warehouseDB.insertProduct(product);
            product.getRequirements().stream().forEach(req -> {
            	Article article = warehouseDB.getArticle(req.getArticleId());
            	article.addRequirement(req);
            });
        }
    }
}
