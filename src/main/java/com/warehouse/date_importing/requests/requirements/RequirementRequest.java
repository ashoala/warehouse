package main.java.com.warehouse.date_importing.requests.requirements;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequirementRequest {
    @JsonProperty("art_id")
    private final int artId;
    @JsonProperty("amount_of")
    private final int amount;

    @JsonCreator
    public RequirementRequest(@JsonProperty("art_id") int artId, @JsonProperty("amount_of") int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Product can not depend on article with 0 or negative amount");
        }
        this.artId = artId;
        this.amount = amount;
    }

    public int getArtId() {
        return artId;
    }

    public int getAmount() {
        return amount;
    }
}
