package main.java.com.warehouse.date_importing.requests.products;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ProductsRequest {
    @JsonProperty("products")
    private List<ProductRequest> productRequests;

    @JsonCreator
    public ProductsRequest(@JsonProperty("products") List<ProductRequest> productRequests) {
        this.productRequests = productRequests;
    }

    public List<ProductRequest> getProductRequests() {
        return productRequests;
    }
}
