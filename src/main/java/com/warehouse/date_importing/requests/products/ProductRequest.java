package main.java.com.warehouse.date_importing.requests.products;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import main.java.com.warehouse.date_importing.requests.requirements.RequirementRequest;

import java.util.List;

public class ProductRequest {
    @JsonProperty("name")
    private final String name;

    @JsonProperty("contain_articles")
    private final List<RequirementRequest> requirementRequests;

    @JsonCreator
    public ProductRequest(
            @JsonProperty("name") String name,
            @JsonProperty("contain_articles") List<RequirementRequest> requirementRequests
    ) {
        if (requirementRequests == null || requirementRequests.isEmpty()) {
            throw new IllegalArgumentException("product should have articles,  key (contain_articles) is mandatory");
        }
        this.name = name;
        this.requirementRequests = requirementRequests;
    }

    public String getName() {
        return name;
    }

    public List<RequirementRequest> getRequirementRequests() {
        return requirementRequests;
    }
}
