package main.java.com.warehouse.date_importing.requests.articles;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InventoryRequest {
    @JsonProperty("inventory")
    private List<ArticleRequest> articleRequests;

    @JsonCreator
    public InventoryRequest(@JsonProperty("inventory") List<ArticleRequest> articleRequests) {
        this.articleRequests = articleRequests;
    }

    public List<ArticleRequest> getArticleRequests() {
        return articleRequests;
    }
}
