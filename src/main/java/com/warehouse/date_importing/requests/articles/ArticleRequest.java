package main.java.com.warehouse.date_importing.requests.articles;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ArticleRequest {
    @JsonProperty("art_id")
    private final int artId;
    @JsonProperty("name")
    private final String name;
    @JsonProperty("stock")
    private int stock;

    @JsonCreator
    public ArticleRequest(
            @JsonProperty("art_id") int artId,
            @JsonProperty("name") String name,
            @JsonProperty("stock") int stock
    ) {
        this.artId = artId;
        this.name = name;
        this.stock = stock;
    }

    public int getArtId() {
        return artId;
    }

    public String getName() {
        return name;
    }

    public int getStock() {
        return stock;
    }
}
