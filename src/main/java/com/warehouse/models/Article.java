package main.java.com.warehouse.models;

import main.java.com.warehouse.date_importing.requests.articles.ArticleRequest;

import java.util.ArrayList;
import java.util.HashSet;

import java.util.List;
import java.util.Set;

public class Article {

	private final int id;
	private final String name;
	private int stock;
	private List<Requirement> requirements = new ArrayList<>();

	public Article(int id, String name, int stock) {
		this.id = id;
		this.name = name;
		this.stock = stock;
	}

	public static Article buildFromArticleRequest(ArticleRequest articleRequest) {
		return new Article(articleRequest.getArtId(), articleRequest.getName(), articleRequest.getStock());
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getStock() {
		return stock;
	}

	public void removeFromStock(int stockToBeRemoved) {
		stock -= stockToBeRemoved;
	}

	public void addRequirement(Requirement requirement) {
		requirements.add(requirement);
	}

	public List<Requirement> getRequirements() {
		return requirements;
	}
}

 