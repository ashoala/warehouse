package main.java.com.warehouse.models;

import main.java.com.warehouse.date_importing.requests.requirements.RequirementRequest;

public class Requirement {

    private int productId;

    private int articleId;

    private int amount;

    public Requirement(int productId, int articleId, int amount) {
        this.productId = productId;
        this.articleId = articleId;
        this.amount = amount;
    }

    public static Requirement buildFromRequirementRequest(int productId, RequirementRequest requirementRequest) {
        return new Requirement(productId, requirementRequest.getArtId(), requirementRequest.getAmount());
    }

    public int getProductId() {
        return productId;
    }

    public int getArticleId() {
        return articleId;
    }

    public int getAmount() {
        return amount;
    }
}
