package main.java.com.warehouse.models;

import main.java.com.warehouse.date_importing.requests.products.ProductRequest;

import java.util.List;
import java.util.stream.Collectors;

public class Product {

	private final int id;
	private final String name;
	private final List<Requirement> requirements;
	private int count;
	
	public Product(int id, String name, List<Requirement> requirements) {
		this.id = id;
		this.name = name;
		this.requirements = requirements;
	}

	public static Product buildFromProductRequest(int productId, ProductRequest productRequest) {
		List<Requirement> requirements = productRequest.getRequirementRequests().stream()
				.map(requirementRequest -> Requirement.buildFromRequirementRequest(productId, requirementRequest))
				.collect(Collectors.toList());
		return new Product(productId, productRequest.getName(), requirements);
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
	
	public List<Requirement> getRequirements() {
		return requirements;
	}


	public synchronized void setCount(int count){
		this.count = count;
	}
	
	public int getCount() {
		return count;
	}

}



