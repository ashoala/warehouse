package main.java.com.warehouse.controller;

import java.util.List;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import main.java.com.warehouse.ProductManager;
import main.java.com.warehouse.WarehouseDB;
import main.java.com.warehouse.models.Product;

@Controller
public class ProductController {
	
	WarehouseDB warehouseDB = WarehouseDB.getInstance();
	ProductManager productManager = new ProductManager(warehouseDB);
	
	@RequestMapping("/home")
	public ModelAndView getAllProducts(){
		
		return new ModelAndView("home", "products", warehouseDB.listProducts());
	}
	
	@RequestMapping(value = "/sellProduct", method = RequestMethod.POST)
	public @ResponseBody List<Product> sellProduct(int productId, int count){
		
		productManager.sellProduct(warehouseDB.getProduct(productId),count);
		return WarehouseDB.getInstance().listProducts();
	}
	
}
