package main.java.com.warehouse;

import main.java.com.warehouse.models.Article;
import main.java.com.warehouse.models.Product;
import main.java.com.warehouse.models.Requirement;

public class ProductManager {
	
	private final WarehouseDB warehouseDB;
	
	public ProductManager(WarehouseDB warehouseDB) {
		this.warehouseDB = warehouseDB;
	}

    public synchronized void sellProduct(Product product, int count) {
	    if (count > product.getCount()) {
	        throw new IllegalArgumentException(String.format("Only %s products available", product.getCount()));
        }
        for (Requirement requirement : product.getRequirements())  {
            Article article = warehouseDB.getArticle(requirement.getArticleId());
            article.removeFromStock(count * requirement.getAmount());
        }
        syncProductCount(product);
    }

    public synchronized void syncCountForAllProducts() {
        for (Product product : warehouseDB.listProducts()) {
            updateProductCount(product);
        }
    }

    public synchronized void syncProductCount(Product product) {
	    updateProductCount(product);
		for (Requirement requirement : product.getRequirements()) {
            Article article = warehouseDB.getArticle(requirement.getArticleId());
            article.getRequirements().forEach(req -> updateProductCount(warehouseDB.getProduct(req.getProductId())));
		}
    }

    private synchronized void updateProductCount(Product product) {
        int minCount = Integer.MAX_VALUE;
        for(Requirement req: product.getRequirements()) {
            Article article  = warehouseDB.getArticle(req.getArticleId());
            int numOfProductsBasedOnArticleStock = article.getStock() / req.getAmount();
            minCount = Math.min(numOfProductsBasedOnArticleStock, minCount);
        }
        product.setCount(minCount);
    }

}
