package main.java.com.warehouse;
import main.java.com.warehouse.models.Article;
import main.java.com.warehouse.models.Product;

import java.util.*;

// Memory cache for simplicity
public class WarehouseDB {


	private static WarehouseDB warehouseDB = new WarehouseDB();
	
	private Map<Integer, Product> products = new HashMap<>();
	private Map<Integer, Article> articles = new HashMap<>();
	
	private WarehouseDB() {}
	
	public static WarehouseDB getInstance() {
		return warehouseDB;
	}
	
	public List<Product> listProducts(){
		return new ArrayList<>(products.values());
	}

	public List<Article> listArticles(){
		return new ArrayList<>(articles.values());
	}
	
	public Product getProduct(int productID) {
		return products.get(productID);
	}

	public Article getArticle(int articleID) {
		return articles.get(articleID);
	}

	public void insertProduct(Product product) {
		if (products.containsKey(product.getId())) {
			throw new IllegalArgumentException(String.format("product %s is already exist in the Db", product.getId()));
		}
		products.put(product.getId(), product);
	}

	public void insertArticle(Article article) {
		if (articles.containsKey(article.getId())) {
			throw new IllegalArgumentException(String.format("article %s is already exist in the Db", article.getId()));
		}
		articles.put(article.getId(), article);
	}

	public int productsSize() {
		return products.size();
	}
}
