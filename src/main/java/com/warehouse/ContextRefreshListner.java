package main.java.com.warehouse;

import java.io.IOException;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.com.warehouse.date_importing.DataImporter;

public class ContextRefreshListner implements ApplicationListener<ContextRefreshedEvent> {
	
	private static String INVENTORY_FILE_PATH = "inventory.json";
	private static String PRODUCTS_FILE_PATH = "products.json";
	
	private final WarehouseDB warehouseDB = WarehouseDB.getInstance();
	private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	if (warehouseDB.productsSize() == 0) {
    		initializeDataFromJsonFiles();
    	}
    }
    
    private void initializeDataFromJsonFiles() {	
    	DataImporter dataImporter = new DataImporter(objectMapper, warehouseDB);
    	ProductManager productManager = new ProductManager(warehouseDB);

		try {
			dataImporter.importArticlesFromFile(INVENTORY_FILE_PATH);
			dataImporter.importProductsFromFile(PRODUCTS_FILE_PATH);
			productManager.syncCountForAllProducts();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
}